<?php

use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('users', 'UserController@index')->name('usuarios');
// Route::get('users/{id}', 'UserController@show');
// Route::get('users/{id}/edit', 'UserController@edit');
// Route::get('users/create', 'UserController@create');
// Route::put('users/{id}', 'UserController@update');
// Route::delete('users/{id}', 'UserController@destroy');
// Route::post('users', 'UserController@store');
// Route::get('users/create', 'UserController@create');

//Ruta de tipo resource: equivale a las 7 rutas REST
//Ruta especial antes que resource, si no "show...."

//Route::get('users/especial', 'UserController@especial');
Route::resource('cathegories','CathegoriesController');
Route::resource('products','ProductController');
Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController');
Route::resource('orders','OrderController');





////////////////////////////////////////////
//Ejemplos de rutas con funciones anónimas:
////////////////////////////////////////////

Route::get('usuarios/{id}', function ($id) {
   return "Detalle del usuario $id";
});


Route::get('usuarios/{id}', function ($id) {
   return "Detalle del usuario $id";
})->where('id', '[0-9]+');

Route::get('usuarios/{id}/{name?}', function ($id, $name=null) {
    if($name) {
        return "Detalle del usuario $id. El nombre es $name";
    } else {
        return "Detalle del usuario $id. Anónimo";
    }
});


Route::get('/hoy',function(){
     $today=Carbon::today();
     return $today->format('d-m-Y');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');





Route::get('/group', 'GroupController@index');
Route::get('/group/flush', 'GroupController@flush');
Route::get('/group/{id}', 'GroupController@addUser');


Route::get('/basket', 'BasketController@index');
Route::get('/basket/flush', 'BasketController@flush');
Route::get('/basket/subtractProduct/{id}','BasketController@subtractProduct');
Route::get('/basket/delete/{id}', 'BasketController@delete');
Route::get('/basket/{id}', 'BasketController@addProduct');
Route::get('/basket/store', 'BasketController@store');

Route::get('/orders/paid/{id}', 'OrderController@paid');
