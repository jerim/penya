@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')

    <h1>
        Este es el detalle del role <?php echo $role->id ?>
    </h1>

    <ul>
        <li>Nombre: {{ $role->name }}</li>

    </ul>

    <h2> Lista de usuarios </h2>
    <ul>
        @foreach ($role->users as $user)
        <li> {{ $user->name }} / {{$user->email}}</li>

        @endforeach
    </ul>

@endsection

