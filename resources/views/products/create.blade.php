@extends('layouts.app')

@section('title', 'Productos')

@section('content')
<style type="text/css">
    .alert {
      padding: 5px;
      background-color: #faa; /* Red */
      margin: 5px;
    }
</style>
    <h1>Alta de productos</h1>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{--
    --}}

    @if ($errors->any())
        <div class="alert alert-danger">
            Se han producido errores de validación
        </div>
    @endif

    <form method="post" action="/products">
        {{ csrf_field() }}

        <label>Nombre</label>
        <input type="text" name="name" value="{{ old('name') }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>

        <label>Precio</label>
        <input type="number" name="price" value="{{ old('price') }}">
        <div class="alert alert-danger">
            {{ $errors->first('price') }}
        </div>

        <br>
       <label> Categoría: </label>
        <select name="cathegory_id">
          @foreach ($cathegories as $cathegories)
           <option value="{{ $cathegories->id }}"
           {{ old('cathegories_id') == $cathegories?
            'selected="selected"' :'' }}>{{ $cathegories->name }}
            </option>
           @endforeach

          <div class="alert alert-danger">
          {{ $errors->first('cathegories_id') }}
         </div>
       </select>

       <br>

        <?php
            $example = ['rojo', 'azul', 'verde'];
            $html = '<hr>';
        ?>
        <select name="color">
        @foreach ($example as $item)
            <option value="{{ $item }}"
            {{ old('color') == $item ?
            'selected="selected"' :
            ''
            }}>{{ $item }}
        </option>
        @endforeach
        </select>
        <br>



        <input class="btn btn-success" type="submit" value="Nuevo">  <a class="btn btn-success" href="/products">Volver a productos</a>
    </form>
@endsection
