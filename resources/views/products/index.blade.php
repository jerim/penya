@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Productos</h1>
      <div class="alert">
        <a href="/products/create" class="btn btn-success">Nuevo</a>
      </div>

      <table  class="table table-striped table-hover table-success">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Categoria</th>

          </tr>
        </thead>


        <tbody>


          @forelse ($products as $product)
           @foreach ($cathegories as $categoria)
           @if($product->cathegory_id == $categoria->id)
          <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->price }}</td>
            <td> {{$categoria->name}}
            </td>
            <td>

              <form method="post" action="/products/{{ $product->id }}">
                <a class="btn btn-success"  role="button"
                href="/products/{{ $product->id }}/edit">
                Editar
              </a>
              <a class="btn btn-success" href="/products/{{$product->id}}" class="btn btn-success">Ver</a>
               <a class="btn btn-success" href="/basket/{{ $product->id}}"
                >añade al carrito </a>
               <form method="post" action="/products/{{ $product->id }}">
                  {{ csrf_field() }}
                 <input type="hidden" name="_method" value="DELETE">
                  <input class="btn btn-danger" type="submit" value="borrar">
               </form>
          </td>
        </tr>
          @endif

          @endforeach

        @empty
        <tr><td colspan="4">No hay productos!!</td></tr>
        @endforelse
      </tbody>
    </table>


  </div>
</div>
</div>
@endsection
