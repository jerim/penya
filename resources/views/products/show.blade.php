@extends('layouts.app')

@section('title', 'Productos')

@section('content')

    <h1>
        Este es el detalle de los productos <?php echo $products->id ?>
    </h1>

    <ul>
        <li>Nombre: {{$products->name}}</li>
        <li>Precio: {{$products->price}}</li>
        <li>
             @foreach ($cathegories as $categoria)
             @if($products->cathegory_id == $categoria->id)
             categorias : {{ $categoria->name}}
             @endif
             @endforeach

        </li>

    </ul>

    <a class="btn btn-success" href="/products">Volver a productos</a>

@endsection

