@extends('layouts.app')

@section('title', 'Categorias')

@section('content')
    <h1>Editar categoria</h1>

    <form method="post" action="/cathegories/{{ $cathegories->id }}">
        {{ csrf_field() }}

        <input type="hidden" name="_method" value="PUT">

        <label>Nombre</label>
        <input type="text" name="name"
        value="{{ old('name') ? old('name') : $cathegories->name }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>

        <br>

        <input class="btn btn-success" type="submit" value="Guardar Cambios"> <a class="btn btn-success" href="/cathegories">Volver a categorias</a>
    </form>
@endsection
