@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Categorias</h1>
      <div class="alert">
        <a href="/cathegories/create" class="btn btn-success">Nuevo</a>
      </div>

      <table  class="table table-striped table-hover table-success">
        <thead>
          <tr>
            <th>Nombre</th>
          </tr>
        </thead>


        <tbody>


          @forelse ($cathegories as $cathegories)
          <tr>
            <td>{{ $cathegories->name }}</td>
            <td>

              <form method="post" action="/cathegories/{{ $cathegories->id }}">
                <a class="btn btn-success"  role="button"
                href="/cathegories/{{ $cathegories->id }}/edit">
                Editar
              </a>
              <a class="btn btn-success" href="/cathegories/{{$cathegories->id}}" class="btn btn-success">Ver</a>
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="DELETE">
              <input type="submit" value="borrar" class="btn btn-danger">
            </form>
          </td>
        </tr>
        @empty
        <tr><td colspan="4">No hay categorias!!</td></tr>
        @endforelse
      </tbody>
    </table>


  </div>
</div>
</div>
@endsection
