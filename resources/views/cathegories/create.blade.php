@extends('layouts.app')

@section('title', 'Categorias')

@section('content')
<style type="text/css">
    .alert {
      padding: 5px;
      background-color: #faa; /* Red */
      margin: 5px;
    }
</style>
    <h1>Alta de Categories</h1>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{--
    --}}

    @if ($errors->any())
        <div class="alert alert-danger">
            Se han producido errores de validación
        </div>
    @endif

    <form method="post" action="/cathegories">
        {{ csrf_field() }}

        <label>Nombre</label>
        <input type="text" name="name" value="{{ old('name') }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>

        <?php
            $example = ['rojo', 'azul', 'verde'];
            $html = '<hr>';
        ?>
        <select name="color">
        @foreach ($example as $item)
            <option value="{{ $item }}"
            {{ old('color') == $item ?
            'selected="selected"' :
            ''
            }}>{{ $item }}
        </option>
        @endforeach
        </select>
        <br>
        <br>


        <input  class="btn btn-success" type="submit" value="Nuevo">  <a class="btn btn-success" href="/cathegories">Volver a categorias</a>
    </form>
@endsection
