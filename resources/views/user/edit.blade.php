@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')
    <h1>Editar usuario</h1>

    <form method="post" action="/users/{{ $user->id }}">
        {{ csrf_field() }}

        <input type="hidden" name="_method" value="PUT">

        <label>Nombre</label>
        <input type="text" name="name"
        value="{{ old('name') ? old('name') : $user->name }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>

        <br>

        <label>Email</label>
        <input type="text" name="email"
        value="{{ old('email') ? old('email') : $user->email }}">
        <div class="alert alert-danger">
            {{ $errors->first('email') }}
        </div>
        <br>
          <label>Contraseña</label>
        <input type="password" name="password" value="{{ old('password') ? old('password') : $user->password }}">
        <div class="alert alert-danger">
            {{ $errors->first('password') }}
        </div>

        <br>

        <input type="submit" value="Guardar Cambios">
    </form>
@endsection
