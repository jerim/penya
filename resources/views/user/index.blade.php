@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Usuarios</h1>
      <div class="alert">
        <a href="/users/create" class="btn btn-success">Nuevo</a>
      </div>

      <table  class="table table-striped table-hover table-success">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Roles</th>
          </tr>
        </thead>


        <tbody>


          @forelse ($users as $user)
          <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td> {{ $user->role->name }} </td>

            <td>

              <form method="post" action="/users/{{ $user->id }}">
                <a class="btn btn-success"  role="button"
                href="/users/{{ $user->id }}/edit">
                Editar
              </a>
                 <a class="btn btn-success" href="/group/{{ $user->id}}">Guardar</a>

              {{ csrf_field() }}
              <input type="hidden" name="_method" value="DELETE">
              <input type="submit" value="borrar" class="btn btn-danger">
            </form>
          </td>
        </tr>
        @empty
        <tr><td colspan="4">No hay Usuarios!!</td></tr>
        @endforelse
      </tbody>
    </table>
    {{ $users->render() }}

  </div>
</div>
</div>
@endsection
