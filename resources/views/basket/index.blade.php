@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Cesta en sesión</h1>

      <table  class="table table-striped table-hover table-success">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Cantidad</th>
            <th>Importe</th>
            <th>Total calculado </th>
          </tr>

        </thead>


        <tbody>


          @forelse ($products as $product)
          <tr>
            <tr>
                <td>{{ $product->name }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->quantity }}</td>
                <td>{{ $product->price * $product->quantity }}</td>
                <td>{{ $product->quantity * $product->price }}</td>

            <td>
             <a href="/basket/subtractProduct/{{ $product->id }}" class="btn btn-danger">
                Quitar
            </a>
          </td>
            <td>
            <a href="/basket/{{ $product->id }}" class="btn btn-success">
                Añade
            </a></td>

          </td>

          </tr>
          @empty
          <tr><td colspan="4">No hay nada en la cesta!!</td></tr>
          @endforelse
        </tbody>
      </table>

      <a class="btn btn-danger" href="/basket/flush">Vacíar cesta</a>
      <a class="btn btn-success" href="/products">Volver a productos</a>
      <a  class="btn btn-success" href="/basket">Confirmar pedido</a>



    </div>
  </div>
</div>
@endsection
