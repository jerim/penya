@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Pedidos de la cesta</h1>

      <table  class="table table-striped table-hover table-success">
        <thead>
          <tr>
            <th>Fecha</th>
            <th>Precio total</th>
            <th>Usuario</th>
            <th>Pagado</th>
            <th>Estatus del pago</th>

          </tr>



        </thead>


        <tbody>
          @forelse ($orders as $order)
        <tr>
            <td>{{ date("d/m/Y", strtotime($order->date)) }}</td>
            <td>{{$order->total() }}</td>
            <td>{{ $order->user->name }}</td>
            <td>@if($order->paid == 0) no @else si @endif</td>
            <td>
              <a href="/orders/{{ $order->id }}">Ver</a>
        </td>
        </tr>

          @empty
          <tr><td colspan="4">No hay nada de pedidos!!</td></tr>
          @endforelse
        </tbody>

      </table>

      <a class="btn btn-danger" href="/basket/flush">Vacíar pedido</a>
      <a class="btn btn-success" href="/products">Volver a productos</a>

    </div>
  </div>
</div>
@endsection
