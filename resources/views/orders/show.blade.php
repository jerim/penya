@extends('layouts.app')

@section('title', 'Cathegories')

@section('content')

<h1>
    Detalle del pedido <?php echo $orders->id ?>
</h1>

<ul>
    <li>Fecha: {{ date("d/m/Y", strtotime($orders->date)) }}</li>
    <li>Total: {{ $orders->total() }}</li>
    <li>Pagado:
        @if($orders->paid == 0) no
            @can('viewAllOrders',$orders)<a href="/orders/paid/{{ $orders->id }}" class="btn btn-primary">Pagar</a>@endcan
        @else Si
        @endif</li>
    <li>Usuario: {{ $orders->name }}</li>
</ul>

<h2>
    Productos del pedido:
</h2>
<ul>
    @foreach($orders->products as $product)
    <li>{{$product->name}} - {{$product->price}} - {{$product->pivot->quantity}} cantidad</li>
    @endforeach
</ul>

@endsection
