<?php

namespace App\Policies;

use App\User;
use App\Cathegories;
use Illuminate\Auth\Access\HandlesAuthorization;

class CathegoriesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the cathegories.
     *
     * @param  \App\User  $user
     * @param  \App\Cathegories  $cathegories
     * @return mixed
     */
    public function view(User $user, Cathegories $cathegories)
    {
        //
    }

    /**
     * Determine whether the user can create cathegories.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the cathegories.
     *
     * @param  \App\User  $user
     * @param  \App\Cathegories  $cathegories
     * @return mixed
     */
    public function update(User $user, Cathegories $cathegories)
    {
        //
    }

    /**
     * Determine whether the user can delete the cathegories.
     *
     * @param  \App\User  $user
     * @param  \App\Cathegories  $cathegories
     * @return mixed
     */
    public function delete(User $user, Cathegories $cathegories)
    {
        //
    }

    /**
     * Determine whether the user can restore the cathegories.
     *
     * @param  \App\User  $user
     * @param  \App\Cathegories  $cathegories
     * @return mixed
     */
    public function restore(User $user, Cathegories $cathegories)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the cathegories.
     *
     * @param  \App\User  $user
     * @param  \App\Cathegories  $cathegories
     * @return mixed
     */
    public function forceDelete(User $user, Cathegories $cathegories)
    {
        //
    }
}
