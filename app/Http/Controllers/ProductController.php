<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cathegories;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct(){

   /*  $this->middleware('auth'); //aplicable a todos los métodos
      $this->middleware('log')->only('index');//solamente a ....
      $this->middleware('subscribed')->except('store');//todos excepto ...*/
       // $this->middleware('auth'); //aplicable a todos los métodos
        $this->middleware('auth');
    }

    public function index()
    {
       $products = Product::paginate(10);

         $cathegories = Cathegories::all();
        return view('products.index', ['products' => $products],['cathegories'=>$cathegories]);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $cathegories =Cathegories::all();
        return view('products.create', ['cathegories'=>$cathegories]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validacion:
        $rules = [
            'name' => 'required|max:255|min:3',
            'price' => 'required|max:255|min:1',
            'cathegories_id' => 'min:1'

        ];

        $request->validate($rules);

        $products = new Product($request->all());
        $products->fill($request->all());
        $products->save();

        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

       $products = Product::findOrFail($id);
        $cathegories = Cathegories::all();
        return view('products.show',['products'=>$products],['cathegories'=>$cathegories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::findOrFail($id);
          $cathegories = Cathegories::all();
        return view('products.edit', ['products' => $products],['cathegories'=>$cathegories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'required|max:255|min:3',
            'price' => 'required |max:255|min:1',
            'cathegories_id' => 'min:1'

        ];

        $request->validate($rules);

        $products = Product::findOrFail($id);
        $products->fill($request->all());
        $products->save();


        return redirect('/products/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Product::destroy($id);

        return back();
    }

    public function especial()
    {
        $products = Product::where('id', '>=', 15)
            ->where('id', '<=', 20)
            ->get();

        dd($products);
        return "especial";
        return redirect('/products');
        // return "Especial";
    }
}
