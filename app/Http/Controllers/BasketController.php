<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\User;
use Session;

class BasketController extends Controller
{

    public function index(Request $request){
       $basket = $request->session()->get('basket');
        if (!$basket) {
            $basket = [];
        }
        return view('basket.index',['products' => $basket]);
        // dd(session::all());
        return $basket;
    }



    public function flush(Request $request){
       $request->session()->forget('basket');

        return back();
    }




    public function addProduct(Request $request,$id){
            // buscar usuario
    $products = Product::findOrFail($id);

     $basket = $request->session()->get('basket');

     if ( $basket == null){
        $basket = array();
    }

    $position = -1;
    foreach ($basket as $key => $item) {
        if ($item->id == $id) {
            $position = $key;
            break;
        }
    }
    if ($position == -1) {
        $products->quantity=1;


        $request->session()->push('basket', $products);
    } else {
        $basket[$position]->quantity++;

    }


    return redirect('/basket');
    }

      public function subtractProduct(Request $request,$id)
    {
        $products = Product::findOrFail($id);

        $basket = $request->session()->get('basket');

        $position = -1;
        foreach ($basket as $key => $products) {
            if($products->id == $id){
                $position = $key;
                break;
            }
        }

        if($position != -1){
            $basket[$position]->quantity--;
            $basket[$position]->quantity <= 0 ? Session::forget('basket.'.$position):"";
        }

    return redirect('/basket');
    }




    public function store(Request $request)
    {
        if($request->session()->get('basket') != null){
                $basket = $request->session()->get('basket');

                $user = \Auth::user();
                $orders = Order::create([
                'date'=>date('Y/m/d') ,
                'user_id'=>$user->id]
                );

                $priceOrder = 0;
                foreach ($basket as $products) {
                    $priceOrder += $products->price * $products->quantity;
                }


                foreach ($basket as $products) {
                    $orders->products()->attach($products->id, ['quantity' => $products->quantity, 'price' => $products->price]);
                }
                $request->session()->forget('basket');

            }return redirect('/basket');
    }

    }



