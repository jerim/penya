<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cathegories;

class CathegoriesController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function __construct(){

   /*  $this->middleware('auth'); //aplicable a todos los métodos
      $this->middleware('log')->only('index');//solamente a ....
      $this->middleware('subscribed')->except('store');//todos excepto ...*/
      $this->middleware('auth')->except('index');
    }

        public function index() {
            $cathegories = Cathegories::paginate(10);
             return view('cathegories.index', ['cathegories' => $cathegories]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function create(){
            return view('cathegories.create');
       }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function store(Request $request){
                //validacion:
            $rules = [
              'name' => 'required|max:255|min:3',
                // 'color' => 'required',
                // 'player' => 'required|max:255|min:5',
            ];

            $request->validate($rules);

            $cathegories = new  Cathegories($request->all());
            $cathegories->fill($request->all());
            $cathegories->save();

            return redirect('/cathegories');
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function show($id){
            $cathegories = Cathegories::findOrFail($id);

            return view('cathegories.show',['cathegories'=>$cathegories]);
        }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function edit($id){
            $cathegories = Cathegories::findOrFail($id);

            return view('cathegories.edit', ['cathegories' => $cathegories]);
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function update(Request $request, $id){

            $rules = [
                'name' => 'required|max:255|min:3',
            ];

            $request->validate($rules);


            $cathegories = Cathegories::findOrFail($id);
            $cathegories->fill($request->all());
            $cathegories->save();


            return redirect('/cathegories/');
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function destroy($id){


            Cathegories::destroy($id);

            return back();
        }

}
