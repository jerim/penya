<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;

class GroupController extends Controller
{
    public function index(Request $request)
    {
        $group = $request->session()->get('group');
        if (! $group) {
            $group = [];
        }
        return view('group.index',['users' => $group]);
        // dd(session::all());
        return $group;
    }




    public function flush(Request $request)
    {
        $request->session()->forget('group');

        return back();
    }



    public function addUser(Request $request,$id)
    {
        // buscar usuario
     $user = User::findOrFail($id);

     $group = $request->session()->get('group');

     if ( $group == null){
        $group = array();
    }

    $position = -1;
    foreach ($group as $key => $item) {
        if ($item->id == $id) {
            $position = $key;
            break;
        }
    }
    if ($position == -1) {
        $user->cantidad =1;
        $request->session()->push('group', $user);
    } else {
        $group[$position]->cantidad++;
    }





    // if(! in_array($user , $group)) {
    //    $request->session()->push('group', $user);
    // }

    // return redirect( '/group');
    //     // $session = Session::all();
    //     // $group = $request->session('group');
    //     // if (!$group){
    //     //     $group = array();
    //     //     session(['group' => $grou]);
    //     // }
    //     // $group[] = $user;

    //     //comprobar si esta en grupo de sesion

    //     //si no está, añadirlo
    return redirect('/group');
}
}
